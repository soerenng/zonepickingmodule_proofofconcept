package PickOrderLoop.Classes;

public class Loop {

    private static Long pickOrderReferenceId = 0L;

    public static Long getNextPickOrderReferenceId() {
        return pickOrderReferenceId++;
    }

    private static Long transportUnitBarcode = 0L;

    public static Long getNextTransportUnitBarcode() {
        return transportUnitBarcode++;
    }

    private void performPick() {
        // Business Logic

        // Notify Loop

        // Notify Stock rebook


        // Notify Tranport Unit Out

    }
}
