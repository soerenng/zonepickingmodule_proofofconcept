package PickOrderLoop.ControllerAdvices;

import PickOrderLoop.Exceptions.PickOrderLineNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;


@ControllerAdvice
class PickOrderLineNotFoundAdvice {

    @ResponseBody
    @ExceptionHandler(PickOrderLineNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String pickOrderLineNotFoundHandler(PickOrderLineNotFoundException ex) {
        return ex.getMessage();
    }
}