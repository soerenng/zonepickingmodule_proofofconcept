package PickOrderLoop.Controllers;

import PickOrderLoop.Classes.Loop;
import PickOrderLoop.Components.PickOrderResourceAssembler;
import PickOrderLoop.Enum.PickOrderState;
import PickOrderLoop.Enum.PickOrderType;
import PickOrderLoop.Exceptions.PickOrderNotFoundException;
import PickOrderLoop.Interfaces.IPickOrderCoordinator;
import PickOrderLoop.Interfaces.IPickOrderReceiver;
import PickOrderLoop.PersistentClasses.PickOrder;
import PickOrderLoop.PersistentClasses.PickOrderLine;
import PickOrderLoop.Repositories.PickOrderLineRepository;
import PickOrderLoop.Repositories.PickOrderRepository;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class LoopController implements IPickOrderReceiver, IPickOrderCoordinator {

    private PickOrderRepository pickOrderRepository;
    private PickOrderLineRepository pickOrderLineRepository;
    private final PickOrderResourceAssembler assembler;

    public LoopController(PickOrderRepository pickOrderRepository,
                          PickOrderLineRepository pickOrderLineRepository,
                          PickOrderResourceAssembler assembler) {
        this.pickOrderRepository = pickOrderRepository;
        this.pickOrderLineRepository = pickOrderLineRepository;
        this.assembler = assembler;
    }

    @Override
    @PostMapping(path = "/add")
    public ResponseEntity<?> receiveNewPickOrder(@RequestBody PickOrder newPickOrder) throws URISyntaxException {

        Resource<PickOrder> resource = assembler.toResource(pickOrderRepository.save(newPickOrder));

        return ResponseEntity
                .created(new URI(resource.getId().expand().getHref()))
                .body(resource);
    }


    @Override
    @GetMapping(path = "/orders")
    public @ResponseBody
    Resources<Resource<PickOrder>> getAllOrders() {
        // This returns a Resource with JSON with the Pick Orders
        List<Resource<PickOrder>> pickOrders = pickOrderRepository.findAll().stream()
                .map(assembler::toResource)
                .collect(Collectors.toList());

        return new Resources<>(pickOrders,
                linkTo(methodOn(LoopController.class).getAllOrders()).withSelfRel());
    }

    @Override
    @GetMapping("/orders/{id}")
    public Resource<PickOrder> findPickOrder(@PathVariable Long id) {

        PickOrder pickOrder = pickOrderRepository.findById(id)
                .orElseThrow(() -> new PickOrderNotFoundException(id));

        return assembler.toResource(pickOrder);
    }

    @GetMapping("/orders/addInZone")
    public Resource<PickOrder> addDefaultOrderInZone() {
        RestTemplate rt = new RestTemplate();
        String url = "http://localhost:8080/orders/add";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        PickOrder pickOrder = new PickOrder(Loop.getNextPickOrderReferenceId(), PickOrderType.Zone, PickOrderState.New, "barcode" + Loop.getNextTransportUnitBarcode(), new ArrayList<>());
        PickOrderLine pickOrderLine1 = new PickOrderLine(PickOrderState.Ready_To_Pick, pickOrder.getPickOrderReferenceId(), 1L, 1L);
        PickOrderLine pickOrderLine2 = new PickOrderLine(PickOrderState.Ready_To_Pick, pickOrder.getPickOrderReferenceId(), 1L, 2L);
        pickOrderLineRepository.save(pickOrderLine1);
        pickOrderLineRepository.save(pickOrderLine2);
        pickOrder.getPickOrderLines().add(pickOrderLine1);
        pickOrder.getPickOrderLines().add(pickOrderLine2);
        pickOrderRepository.save(pickOrder);


        HttpEntity<PickOrder> request = new HttpEntity<PickOrder>(pickOrder, headers);

        return assembler.toResource(rt.postForObject(url, request, PickOrder.class));
    }

    @Override
    @PostMapping("/orders/update")
    public ResponseEntity<?> updatePickOrder(@RequestBody PickOrder newPickOrder) throws URISyntaxException {

        PickOrder pickOrder = pickOrderRepository.findByPickOrderReferenceId(newPickOrder.getPickOrderReferenceId())
                .map(updateLocalPickOrder(newPickOrder))
                .orElseThrow(() -> new PickOrderNotFoundException(newPickOrder.getPickOrderReferenceId()));

        Resource<PickOrder> resource = assembler.toResource(pickOrder);

        return ResponseEntity
                .created(new URI(resource.getId().expand().getHref()))
                .body(resource);
    }

    private Function<PickOrder, PickOrder> updateLocalPickOrder(@RequestBody PickOrder newPickOrder) {
        return po -> {
            pickOrderLineRepository.saveAll(newPickOrder.getPickOrderLines());
            return pickOrderRepository.save(newPickOrder);
        };
    }
}
