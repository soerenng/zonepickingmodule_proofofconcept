package PickOrderLoop.Exceptions;

public class PickOrderLineNotFoundException extends RuntimeException {

    public PickOrderLineNotFoundException(Long id) {
        super("Could not find Pick Order " + id);
    }
}