package PickOrderLoop.Exceptions;

public class PickOrderNotFoundException extends RuntimeException {

    public PickOrderNotFoundException(Long id) {
        super("Could not find Pick Order " + id);
    }
}