package PickOrderLoop.Interfaces;

import PickOrderLoop.PersistentClasses.PickOrder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

import java.net.URISyntaxException;

public interface IPickOrderCoordinator {
    public ResponseEntity<?> updatePickOrder(@RequestBody PickOrder newPickOrder) throws URISyntaxException;
}
