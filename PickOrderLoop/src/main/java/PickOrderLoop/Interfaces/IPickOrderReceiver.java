package PickOrderLoop.Interfaces;

import PickOrderLoop.PersistentClasses.PickOrder;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.net.URISyntaxException;

public interface IPickOrderReceiver {

    public ResponseEntity<?> receiveNewPickOrder(@RequestBody PickOrder newPickOrder) throws URISyntaxException;

    public @ResponseBody
    Resources<Resource<PickOrder>> getAllOrders();

    public Resource<PickOrder> findPickOrder(@PathVariable Long id);
}
