package PickOrderLoop.PersistentClasses;

import PickOrderLoop.Enum.PickOrderState;
import PickOrderLoop.Enum.PickOrderType;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.List;

@Data
@Entity
public class PickOrder {

    @Id
    @GeneratedValue
    private Long id;
    private PickOrderType type;
    private PickOrderState state;
    private String transportUnitBarcode;
    private Long pickOrderReferenceId;

    @ElementCollection
    private List<PickOrderLine> pickOrderLines;

    public PickOrder(@JsonProperty("pickOrderReferenceId") Long pickOrderReferenceId,
                     @JsonProperty("type") PickOrderType type,
                     @JsonProperty("state") PickOrderState state,
                     @JsonProperty("transportUnitBarcode") String transportUnitId,
                     @JsonProperty("pickOrderLines") List<PickOrderLine> pickOrderLines) {
        this.pickOrderReferenceId = pickOrderReferenceId;
        this.type = type;
        this.state = state;
        this.transportUnitBarcode = transportUnitId;
        this.pickOrderLines = pickOrderLines;
    }

    public PickOrder() {
    }
}
