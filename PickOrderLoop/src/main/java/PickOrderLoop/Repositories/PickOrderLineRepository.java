package PickOrderLoop.Repositories;

import PickOrderLoop.PersistentClasses.PickOrderLine;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PickOrderLineRepository extends JpaRepository<PickOrderLine, Long> {

}
