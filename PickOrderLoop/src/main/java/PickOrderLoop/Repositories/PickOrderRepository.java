package PickOrderLoop.Repositories;

import PickOrderLoop.PersistentClasses.PickOrder;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface PickOrderRepository extends JpaRepository<PickOrder, Long> {

    Optional<PickOrder> findByPickOrderReferenceId(Long pickOrderReferenceId);
}
