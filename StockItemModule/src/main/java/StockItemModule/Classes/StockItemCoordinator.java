package StockItemModule.Classes;

import StockItemModule.PersistentClasses.StockItem;
import StockItemModule.Repositories.StockItemRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

@Component
public class StockItemCoordinator {

    private StockItemRepository stockItemRepository;

    public StockItemCoordinator(StockItemRepository stockItemRepository) {
        this.stockItemRepository = stockItemRepository;
    }

    public Optional<StockItem> findStockItemById(Long id) {
        return stockItemRepository.findById(id);
    }

    public List<StockItem> findAllStockItems() {
        return stockItemRepository.findAll();
    }

    public StockItem findStockItemByMaterialIdAndQuantity(Long materialId, Long quantity) {
        return stockItemRepository.findFirstByMaterialIdAndQuantityGreaterThanEqual(materialId, quantity);
    }

    public StockItem createNewStockItem(StockItem stockItem) {
        return stockItemRepository.save(stockItem);
    }

    public StockItem updateStockItem(StockItem stockItem) {
        return stockItemRepository.save(stockItem);
    }

    public Function<StockItem, StockItem> updateOrCreateStockItem(StockItem newStockItem) {
        return stockItem -> stockItemRepository.save(newStockItem);
    }
}
