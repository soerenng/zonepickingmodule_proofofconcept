package StockItemModule.Components;

import StockItemModule.Controllers.StockItemController;
import StockItemModule.PersistentClasses.StockItem;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class StockItemResourceAssembler implements ResourceAssembler<StockItem, Resource<StockItem>> {


    @Override
    public Resource<StockItem> toResource(StockItem stockItem) {

        return new Resource<>(stockItem,
                linkTo(methodOn(StockItemController.class).findStockItem(stockItem.getId())).withSelfRel(),
                linkTo(methodOn(StockItemController.class).getAllStockItems()).withRel("stockItems"));
    }
}
