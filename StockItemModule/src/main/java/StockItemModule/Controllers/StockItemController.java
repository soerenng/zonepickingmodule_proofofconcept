package StockItemModule.Controllers;

import StockItemModule.Classes.StockItemCoordinator;
import StockItemModule.Components.StockItemResourceAssembler;
import StockItemModule.Exceptions.StockItemNotFoundException;
import StockItemModule.Interfaces.IStockItemInfo;
import StockItemModule.Interfaces.IStockItemSender;
import StockItemModule.Interfaces.IStockItemUpdater;
import StockItemModule.PersistentClasses.StockItem;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class StockItemController implements IStockItemSender, IStockItemUpdater, IStockItemInfo {

    private final StockItemResourceAssembler assembler;
    private StockItemCoordinator stockItemCoordinator;

    public StockItemController(StockItemResourceAssembler assembler, StockItemCoordinator stockItemCoordinator) {
        this.assembler = assembler;
        this.stockItemCoordinator = stockItemCoordinator;
    }

    @Override
    @GetMapping("/stockItems/{id}")
    public Resource<StockItem> findStockItem(@PathVariable Long id) {
        StockItem stockItem = stockItemCoordinator.findStockItemById(id)
                .orElseThrow(() -> new StockItemNotFoundException(id));

        return assembler.toResource(stockItem);
    }

    @Override
    @GetMapping("/stockItems")
    public @ResponseBody
    Resources<Resource<StockItem>> getAllStockItems() {
        // This returns a Resource with JSON with the Transport Units
        List<Resource<StockItem>> transportUnit = stockItemCoordinator.findAllStockItems().stream()
                .map(assembler::toResource)
                .collect(Collectors.toList());

        return new Resources<>(transportUnit,
                linkTo(methodOn(StockItemController.class).getAllStockItems()).withSelfRel());
    }

    @Override
    @GetMapping("/stockItems/{materialId}/{requestedQuantity}")
    public Resource<StockItem> receiveNewStockItemRequest(@PathVariable Long materialId, @PathVariable Long requestedQuantity) throws URISyntaxException {
        return assembler.toResource(stockItemCoordinator.findStockItemByMaterialIdAndQuantity(materialId, requestedQuantity));

    }

    @PostMapping("/stockItems")
    public ResponseEntity<?> receiveStockItem(@RequestBody StockItem newStockItem) throws URISyntaxException {

        Resource<StockItem> resource = assembler.toResource(stockItemCoordinator.createNewStockItem(newStockItem));

        return ResponseEntity
                .created(new URI(resource.getId().expand().getHref()))
                .body(resource);
    }

    @Override
    @PostMapping("/stockItems/update")
    // TODO: Update mapping to be rest
    public ResponseEntity<?> receiveStockItemUpdate(StockItem newStockItem) throws URISyntaxException {
        StockItem stockItem = stockItemCoordinator.findStockItemById(newStockItem.getId())
                .map(stockItemCoordinator.updateOrCreateStockItem(newStockItem))
                .orElseThrow(() -> new StockItemNotFoundException(newStockItem.getId()));

        Resource<StockItem> resource = assembler.toResource(stockItem);

        return ResponseEntity
                .created(new URI(resource.getId().expand().getHref()))
                .body(resource);
    }


}
