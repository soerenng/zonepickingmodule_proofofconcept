package StockItemModule.Exceptions;


public class StockItemNotFoundException extends RuntimeException {

    public StockItemNotFoundException(Long id) {
        super("Could not find Stock Item: " + id);
    }
}