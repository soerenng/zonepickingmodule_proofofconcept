package StockItemModule.Interfaces;

import StockItemModule.PersistentClasses.StockItem;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

public interface IStockItemInfo {

    public Resource<StockItem> findStockItem(@PathVariable Long id);

    public @ResponseBody
    Resources<Resource<StockItem>> getAllStockItems();
}
