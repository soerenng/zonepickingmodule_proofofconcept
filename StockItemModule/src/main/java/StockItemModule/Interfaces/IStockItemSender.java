package StockItemModule.Interfaces;

import StockItemModule.PersistentClasses.StockItem;
import org.springframework.hateoas.Resource;
import org.springframework.web.bind.annotation.PathVariable;

import java.net.URISyntaxException;

public interface IStockItemSender {

    public Resource<StockItem> receiveNewStockItemRequest(@PathVariable Long materialId, @PathVariable Long requestedQuantity) throws URISyntaxException;

}
