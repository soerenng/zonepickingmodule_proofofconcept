package StockItemModule.Interfaces;

import StockItemModule.PersistentClasses.StockItem;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

import java.net.URISyntaxException;

public interface IStockItemUpdater {
    public ResponseEntity<?> receiveStockItemUpdate(@RequestBody StockItem stockItem) throws URISyntaxException;

}
