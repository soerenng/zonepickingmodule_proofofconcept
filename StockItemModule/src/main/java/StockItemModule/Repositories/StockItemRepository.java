package StockItemModule.Repositories;

import StockItemModule.PersistentClasses.StockItem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StockItemRepository extends JpaRepository<StockItem, Long> {

    public StockItem findFirstByMaterialIdAndQuantityGreaterThanEqual(Long materialId, Long quantity);

}
