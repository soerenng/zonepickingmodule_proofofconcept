package TransportUnitModule.Classes;

import TransportUnitModule.PersistentClasses.TransportUnit;
import TransportUnitModule.Repositories.TransportUnitRepository;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class TransportUnitCoordinator {

    private TransportUnitRepository transportUnitRepository;

    public TransportUnitCoordinator(TransportUnitRepository transportUnitRepository) {
        this.transportUnitRepository = transportUnitRepository;
    }

    private Long transportUnitBarcode = 0L;

    public Long getNextTransportUnitBarcode() {
        return transportUnitBarcode++;
    }

    public TransportUnit createrOrUpdateTransportUnit(TransportUnit transportUnit) {
        return transportUnitRepository.save(transportUnit);
    }

    public List<TransportUnit> findAllTransportUnits() {
        return transportUnitRepository.findAll();
    }

    public Optional<TransportUnit> findTransportUnitById(Long id) {
        return transportUnitRepository.findById(id);
    }

}
