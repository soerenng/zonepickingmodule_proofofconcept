package TransportUnitModule.Components;

import TransportUnitModule.Controllers.TransportUnitController;
import TransportUnitModule.PersistentClasses.TransportUnit;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class TransportUnitResourceAssembler implements ResourceAssembler<TransportUnit, Resource<TransportUnit>> {

    @Override
    public Resource<TransportUnit> toResource(TransportUnit transportUnit) {

        return new Resource<>(transportUnit,
                linkTo(methodOn(TransportUnitController.class).findTransportUnit(transportUnit.getId())).withSelfRel(),
                linkTo(methodOn(TransportUnitController.class).getAllTransportUnits()).withRel("transportUnits"));
    }

}
