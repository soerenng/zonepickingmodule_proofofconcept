package TransportUnitModule.Controllers;

import TransportUnitModule.Classes.TransportUnitCoordinator;
import TransportUnitModule.Components.TransportUnitResourceAssembler;
import TransportUnitModule.Enum.TransportUnitLocation;
import TransportUnitModule.Enum.TransportUnitType;
import TransportUnitModule.Exceptions.TransportUnitNotFoundException;
import TransportUnitModule.Interfaces.ITransportUnitReceiver;
import TransportUnitModule.PersistentClasses.TransportUnit;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class TransportUnitController implements ITransportUnitReceiver {

    private final TransportUnitResourceAssembler assembler;
    private TransportUnitCoordinator transportUnitCoordinator;

    public TransportUnitController(TransportUnitResourceAssembler assembler, TransportUnitCoordinator transportUnitCoordinator) {
        this.assembler = assembler;
        this.transportUnitCoordinator = transportUnitCoordinator;
    }

    @Override
    @PostMapping("transportUnits")
    public ResponseEntity<?> receiveTransportUnit(@RequestBody TransportUnit newTransportUnit) throws URISyntaxException {

        Resource<TransportUnit> resource = assembler.toResource(transportUnitCoordinator.createrOrUpdateTransportUnit(newTransportUnit));

        transportUnitCoordinator.createrOrUpdateTransportUnit(newTransportUnit);

        return ResponseEntity
                .created(new URI(resource.getId().expand().getHref()))
                .body(resource);
    }

    @Override
    @GetMapping("/transportUnits/{id}")
    public Resource<TransportUnit> findTransportUnit(@PathVariable Long id) {
        TransportUnit transportUnit = transportUnitCoordinator.findTransportUnitById(id)
                .orElseThrow(() -> new TransportUnitNotFoundException(id));

        return assembler.toResource(transportUnit);
    }

    @Override
    @GetMapping("/transportUnits")
    public @ResponseBody
    Resources<Resource<TransportUnit>> getAllTransportUnits() {
        // This returns a Resource with JSON with the Transport Units
        List<Resource<TransportUnit>> transportUnit = transportUnitCoordinator.findAllTransportUnits().stream()
                .map(assembler::toResource)
                .collect(Collectors.toList());

        return new Resources<>(transportUnit,
                linkTo(methodOn(TransportUnitController.class).getAllTransportUnits()).withSelfRel());
    }

    @GetMapping("/transportUnits/addInZone")
    public void addDefaultTransportUnitInZone() {
        RestTemplate rt = new RestTemplate();
        String url = "http://localhost:8080/transportUnits";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        TransportUnit transportUnit = new TransportUnit(TransportUnitType.Bin, "barcode" + transportUnitCoordinator.getNextTransportUnitBarcode(), TransportUnitLocation.onZoneStation);

        transportUnitCoordinator.createrOrUpdateTransportUnit(transportUnit);


        HttpEntity<TransportUnit> request = new HttpEntity<>(transportUnit, headers);

        rt.postForObject(url, request, String.class);
    }
}
