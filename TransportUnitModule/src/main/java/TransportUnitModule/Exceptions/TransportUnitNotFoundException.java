package TransportUnitModule.Exceptions;

public class TransportUnitNotFoundException extends RuntimeException {

    public TransportUnitNotFoundException(Long id) {
        super("Could not find Transport Unit: " + id);
    }
}