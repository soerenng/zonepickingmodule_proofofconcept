package TransportUnitModule.Interfaces;

import TransportUnitModule.PersistentClasses.TransportUnit;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.net.URISyntaxException;

public interface ITransportUnitReceiver {

    public ResponseEntity<?> receiveTransportUnit(@RequestBody TransportUnit transportUnit) throws URISyntaxException;

    public Resource<TransportUnit> findTransportUnit(@PathVariable Long id);

    public @ResponseBody
    Resources<Resource<TransportUnit>> getAllTransportUnits();

}
