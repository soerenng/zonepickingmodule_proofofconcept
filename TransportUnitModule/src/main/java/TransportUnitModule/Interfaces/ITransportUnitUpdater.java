package TransportUnitModule.Interfaces;

import TransportUnitModule.PersistentClasses.TransportUnit;
import org.springframework.hateoas.Resource;
import org.springframework.web.bind.annotation.RequestBody;

public interface ITransportUnitUpdater {

    public Resource<TransportUnit> updateTransportUnit(@RequestBody TransportUnit transportUnit);

}
