package TransportUnitModule.PersistentClasses;

import TransportUnitModule.Enum.TransportUnitLocation;
import TransportUnitModule.Enum.TransportUnitType;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity
public class TransportUnit {

    @Id
    @GeneratedValue
    private Long id;
    private TransportUnitType type;
    private String barcode;
    private TransportUnitLocation location;

    public TransportUnit(@JsonProperty("type") TransportUnitType type,
                         @JsonProperty("barcode") String barcode,
                         @JsonProperty("location") TransportUnitLocation location) {

        this.type = type;
        this.barcode = barcode;
        this.location = location;
    }

    public TransportUnit() {
    }

}
