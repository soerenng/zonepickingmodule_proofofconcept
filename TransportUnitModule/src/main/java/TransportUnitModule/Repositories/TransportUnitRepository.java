package TransportUnitModule.Repositories;

import TransportUnitModule.PersistentClasses.TransportUnit;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TransportUnitRepository extends JpaRepository<TransportUnit, Long> {

    Optional<TransportUnit> findByBarcode(String barcode);
}
