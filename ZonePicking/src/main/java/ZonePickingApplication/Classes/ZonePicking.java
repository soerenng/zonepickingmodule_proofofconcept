package ZonePickingApplication.Classes;

import ZonePickingApplication.Controllers.PickOrderController;
import ZonePickingApplication.Controllers.StockItemController;
import ZonePickingApplication.Controllers.TransportUnitController;
import ZonePickingApplication.Enum.PickOrderState;
import ZonePickingApplication.Exceptions.PickOrderNotFoundException;
import ZonePickingApplication.Exceptions.TransportUnitNotFoundException;
import ZonePickingApplication.PersistentClasses.PickOrder;
import ZonePickingApplication.PersistentClasses.PickOrderLine;
import ZonePickingApplication.PersistentClasses.StockItem;
import ZonePickingApplication.PersistentClasses.TransportUnit;
import ZonePickingApplication.Repositories.PickOrderLineRepository;
import ZonePickingApplication.Repositories.PickOrderRepository;
import ZonePickingApplication.Repositories.StockItemRepository;
import ZonePickingApplication.Repositories.TransportUnitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class ZonePicking {

    @Autowired
    private PickOrderController pickOrderController;

    @Autowired
    private StockItemController stockItemController;

    @Autowired
    private TransportUnitController transportUnitController;

    private PickOrderRepository pickOrderRepository;
    private PickOrderLineRepository pickOrderLineRepository;
    private TransportUnitRepository transportUnitRepository;
    private StockItemRepository stockItemRepository;

    public ZonePicking(PickOrderRepository pickOrderRepository,
                       PickOrderLineRepository pickOrderLineRepository,
                       TransportUnitRepository transportUnitRepository,
                       StockItemRepository stockItemRepository) {
        this.pickOrderRepository = pickOrderRepository;
        this.pickOrderLineRepository = pickOrderLineRepository;
        this.transportUnitRepository = transportUnitRepository;
        this.stockItemRepository = stockItemRepository;
    }

    public TransportUnit createNewTransportUnit(TransportUnit transportUnit) {
        return transportUnitRepository.save(transportUnit);
    }

    public void newTransportUnitArrived(String transportUnitBarcode) {
        findAndPickOrder(transportUnitBarcode);
    }

    private void findAndPickOrder(String transportUnitBarcode) {
        List<PickOrder> pickOrderForTU = findPickOrderForTU(transportUnitBarcode);
        if (!pickOrderForTU.isEmpty()) {
            for (PickOrder pickOrder : pickOrderForTU) {
                for (PickOrderLine pickOrderLine : pickOrder.getPickOrderLines()) {
                    performPick(pickOrderLine);
                }
                updatePickOrderState(pickOrder);
                pickOrderController.updatePickOrder(pickOrder.getPickOrderReferenceId());
            }
        }
    }

    private List<PickOrder> findPickOrderForTU(String transportUnitBarcode) {
        return pickOrderRepository.findByTransportUnitBarcode(transportUnitBarcode);
    }

    public PickOrder createNewPickOrder(PickOrder pickOrder) {
        for (PickOrderLine pickOrderLine : pickOrder.getPickOrderLines()) {
            pickOrderLineRepository.save(pickOrderLine);
        }
        return pickOrderRepository.save(pickOrder);
    }

    public List<PickOrder> findAllPickOrders() {
        return pickOrderRepository.findAll();
    }

    public PickOrder findPickOrdersById(long id) {
        return pickOrderRepository.findById(id).orElseThrow(() -> new PickOrderNotFoundException(id));
    }

    public PickOrder findPickOrdersByReferenceId(long id) {
        return pickOrderRepository.findByPickOrderReferenceId(id).orElseThrow(() -> new PickOrderNotFoundException(id));
    }

    public Optional<PickOrderLine> findPickOrderLineById(Long id) {
        return pickOrderLineRepository.findById(id);
    }


    public void deletePickOrder(long id) {
        pickOrderRepository.deleteById(id);
    }


    private void performPick(PickOrderLine pickOrderLine) {
        // TODO: This logic is very simplified

        StockItem stockItem = stockItemController.requestStockItem(pickOrderLine.getMaterialId(), pickOrderLine.getRequestedQuantity());
        if (stockItem.getQuantity() >= pickOrderLine.getRequestedQuantity()) {
            stockItem.setQuantity(stockItem.getQuantity() - pickOrderLine.getRequestedQuantity());

            stockItemController.updateStockItem(stockItem);
            stockItemController.updateStockItem(new StockItem(stockItem.getMaterialId(), pickOrderLine.getRequestedQuantity(),
                    findPickOrdersByReferenceId(pickOrderLine.getPickOrderReferenceId()).getTransportUnitBarcode()));

            pickOrderLine.setState(PickOrderState.Picked);
            pickOrderLineRepository.save(pickOrderLine);
        } else {
            System.out.println("Not enough stock to pick Pick Order Line: " + pickOrderLine.getId());
        }

    }

    public TransportUnit findTransportUnitByBarcode(String barcode) {
        return transportUnitRepository.findByBarcode(barcode).orElseThrow(() -> new TransportUnitNotFoundException(barcode));
    }

    public List<TransportUnit> findAllTransportUnits() {
        return transportUnitRepository.findAll();
    }

    private void updatePickOrderState(PickOrder pickOrder) {

        boolean pickOrderDone = true;
        for (PickOrderLine pickOrderLine : pickOrder.getPickOrderLines()) {
            if (!pickOrderLine.getState().equals(PickOrderState.Picked)) {
                pickOrderDone = false;
            }
        }
        if (pickOrderDone) {
            pickOrder.setState(PickOrderState.Picked);
            transportUnitController.updateTransportUnit(pickOrder.getTransportUnitBarcode());
        }
        // TODO: Perhaps this should happen after service has responded to the update
        pickOrderRepository.save(pickOrder);
    }

    public StockItem updateStockItem(StockItem stockItem) {
        return stockItemRepository.save(stockItem);
    }

    public TransportUnit updateTransportUnit(TransportUnit updatedTransportUnit) {
        // TODO: These should have same ID, but to be sure we find the tu by barcode
        return transportUnitRepository.save(findTransportUnitByBarcode(updatedTransportUnit.getBarcode()));
    }

}
