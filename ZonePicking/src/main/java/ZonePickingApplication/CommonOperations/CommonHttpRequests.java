package ZonePickingApplication.CommonOperations;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.lang.Nullable;
import org.springframework.web.client.RestTemplate;

public class CommonHttpRequests {

    public static <T> T postAsJson(T putObject, @Nullable RestTemplate rt, String url) {
        if (rt == null) {
            rt = new RestTemplate();
        }

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<T> request = new HttpEntity<T>(putObject, headers);

        return (T) rt.postForObject(url, request, putObject.getClass());
    }
}
