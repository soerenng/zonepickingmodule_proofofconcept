package ZonePickingApplication.Components;

import ZonePickingApplication.Controllers.PickOrderController;
import ZonePickingApplication.PersistentClasses.PickOrder;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.stereotype.Component;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Component
public class PickOrderResourceAssembler implements ResourceAssembler<PickOrder, Resource<PickOrder>> {

    @Override
    public Resource<PickOrder> toResource(PickOrder pickOrder) {

        return new Resource<>(pickOrder,
                linkTo(methodOn(PickOrderController.class).findPickOrder(pickOrder.getId())).withSelfRel(),
                linkTo(methodOn(PickOrderController.class).getAllOrders()).withRel("orders"));
    }

}
