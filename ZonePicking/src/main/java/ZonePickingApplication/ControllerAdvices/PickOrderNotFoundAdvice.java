package ZonePickingApplication.ControllerAdvices;

import ZonePickingApplication.Exceptions.PickOrderNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
class PickOrderNotFoundAdvice {

    @ResponseBody
    @ExceptionHandler(PickOrderNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String pickOrderNotFoundHandler(PickOrderNotFoundException ex) {
        return ex.getMessage();
    }
}