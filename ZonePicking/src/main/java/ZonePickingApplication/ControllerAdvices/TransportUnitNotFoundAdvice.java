package ZonePickingApplication.ControllerAdvices;

import ZonePickingApplication.Exceptions.TransportUnitNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class TransportUnitNotFoundAdvice {

    @ResponseBody
    @ExceptionHandler(TransportUnitNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    String transportUnitNotFoundHandler(TransportUnitNotFoundException ex) {
        return ex.getMessage();
    }
}
