package ZonePickingApplication.Controllers;

import ZonePickingApplication.Classes.ZonePicking;
import ZonePickingApplication.CommonOperations.CommonHttpRequests;
import ZonePickingApplication.Components.PickOrderResourceAssembler;
import ZonePickingApplication.Interfaces.IPickOrderPicker;
import ZonePickingApplication.Interfaces.IPickOrderReceiver;
import ZonePickingApplication.PersistentClasses.PickOrder;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class PickOrderController implements IPickOrderReceiver, IPickOrderPicker {

    private final PickOrderResourceAssembler assembler;
    private ZonePicking zonePicking;

    public PickOrderController(PickOrderResourceAssembler assembler, ZonePicking zonePicking) {
        this.assembler = assembler;
        this.zonePicking = zonePicking;
    }

    @Override
    @PostMapping("orders/add")
    public ResponseEntity<?> receiveNewPickOrder(@RequestBody PickOrder newPickOrder) throws URISyntaxException {

        Resource<PickOrder> resource = assembler.toResource(zonePicking.createNewPickOrder(newPickOrder));

        return ResponseEntity
                .created(new URI(resource.getId().expand().getHref()))
                .body(resource);
    }

    @Override
    @GetMapping("/orders")
    public @ResponseBody
    Resources<Resource<PickOrder>> getAllOrders() {
        // This returns a Resource with JSON with the Pick Orders
        List<Resource<PickOrder>> pickOrders = zonePicking.findAllPickOrders().stream()
                .map(assembler::toResource)
                .collect(Collectors.toList());

        return new Resources<>(pickOrders,
                linkTo(methodOn(PickOrderController.class).getAllOrders()).withSelfRel());
    }


    @Override
    @GetMapping("/orders/{id}")
    public Resource<PickOrder> findPickOrder(@PathVariable Long id) {

        PickOrder pickOrder = zonePicking.findPickOrdersById(id);

        return assembler.toResource(pickOrder);
    }

    @Override
    @DeleteMapping("/orders/{id}")
    public ResponseEntity<?> deletePickOrder(Long id) {
        zonePicking.deletePickOrder(id);

        return ResponseEntity.noContent().build();
    }

    @Override
    public void updatePickOrder(@PathVariable Long id) {
        PickOrder pickOrder = zonePicking.findPickOrdersByReferenceId(id);

        String url = "http://localhost:8081/orders/update";
        CommonHttpRequests.postAsJson(pickOrder, null, url);
    }

}
