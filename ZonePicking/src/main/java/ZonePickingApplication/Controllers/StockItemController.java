package ZonePickingApplication.Controllers;

import ZonePickingApplication.Classes.ZonePicking;
import ZonePickingApplication.CommonOperations.CommonHttpRequests;
import ZonePickingApplication.Interfaces.IStockItemRequester;
import ZonePickingApplication.Interfaces.IStockItemUpdater;
import ZonePickingApplication.PersistentClasses.StockItem;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class StockItemController implements IStockItemRequester, IStockItemUpdater {

    private ZonePicking zonePicking;

    public StockItemController(ZonePicking zonePicking) {
        this.zonePicking = zonePicking;
    }

    @Override
    public StockItem requestStockItem(Long materialId, Long requestedQuantity) {
        RestTemplate rt = new RestTemplate();
        String url = "http://localhost:8083/stockItems/" + materialId + "/" + requestedQuantity;
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        HttpEntity<?> request = new HttpEntity<>(headers);

        final StockItem requestedStockItem = rt.getForObject(url, StockItem.class, request);

        if (requestedStockItem != null) {
            return requestedStockItem;
        } else {
            // TODO: What to do in this case?
        }

        return new StockItem(null, null, null);
    }

    @Override
    public void updateStockItem(StockItem stockItem) {
        // TODO: Send stock item to service that should update it
        String url = "http://localhost:8083/stockItems";

        final StockItem updatedStockItem = CommonHttpRequests.postAsJson(stockItem, null, url);

        zonePicking.updateStockItem(updatedStockItem);
    }


}
