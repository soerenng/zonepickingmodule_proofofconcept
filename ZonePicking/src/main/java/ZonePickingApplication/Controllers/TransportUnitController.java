package ZonePickingApplication.Controllers;

import ZonePickingApplication.Classes.ZonePicking;
import ZonePickingApplication.CommonOperations.CommonHttpRequests;
import ZonePickingApplication.Components.TransportUnitResourceAssembler;
import ZonePickingApplication.Enum.TransportUnitLocation;
import ZonePickingApplication.Interfaces.ITransportUnitReceiver;
import ZonePickingApplication.Interfaces.ITransportUnitUpdater;
import ZonePickingApplication.PersistentClasses.TransportUnit;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class TransportUnitController implements ITransportUnitReceiver, ITransportUnitUpdater {

    private final TransportUnitResourceAssembler assembler;
    private ZonePicking zonePicking;

    public TransportUnitController(TransportUnitResourceAssembler assembler, ZonePicking zonePicking) {
        this.assembler = assembler;
        this.zonePicking = zonePicking;
    }

    @Override
    public void updateTransportUnit(String barcode) {
        TransportUnit transportUnit = zonePicking.findTransportUnitByBarcode(barcode);

        transportUnit.setLocation(TransportUnitLocation.onConveyor);

        String url = "http://localhost:8082/transportUnits/update";
        final TransportUnit updatedTransportUnit = CommonHttpRequests.postAsJson(transportUnit, null, url);
        zonePicking.updateTransportUnit(updatedTransportUnit);
    }

    @Override
    @PostMapping("transportUnits")
    public ResponseEntity<?> receiveNewTransportUnit(@RequestBody TransportUnit newTransportUnit) throws URISyntaxException {

        Resource<TransportUnit> resource = assembler.toResource(zonePicking.createNewTransportUnit(newTransportUnit));

        zonePicking.newTransportUnitArrived(newTransportUnit.getBarcode());

        return ResponseEntity
                .created(new URI(resource.getId().expand().getHref()))
                .body(resource);
    }

    @Override
    @GetMapping("/transportUnits/{barcode}")
    public Resource<TransportUnit> findTransportUnit(@PathVariable String barcode) {
        TransportUnit transportUnit = zonePicking.findTransportUnitByBarcode(barcode);

        return assembler.toResource(transportUnit);
    }

    @Override
    @GetMapping("/transportUnits")
    public @ResponseBody
    Resources<Resource<TransportUnit>> getAllTransportUnits() {
        // This returns a Resource with JSON with the Transport Units
        List<Resource<TransportUnit>> transportUnit = zonePicking.findAllTransportUnits().stream()
                .map(assembler::toResource)
                .collect(Collectors.toList());

        return new Resources<>(transportUnit,
                linkTo(methodOn(TransportUnitController.class).getAllTransportUnits()).withSelfRel());
    }

}
