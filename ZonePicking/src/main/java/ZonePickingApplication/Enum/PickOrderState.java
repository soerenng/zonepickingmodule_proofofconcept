package ZonePickingApplication.Enum;

public enum PickOrderState {
    New, Released, Ready_To_Pick, Picked, Short_Picked;

}
