package ZonePickingApplication.Exceptions;

public class TransportUnitNotFoundException extends RuntimeException {

    public TransportUnitNotFoundException(String barcode) {
        super("Could not find Transport Unit: " + barcode);
    }
}