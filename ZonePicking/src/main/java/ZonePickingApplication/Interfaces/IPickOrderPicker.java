package ZonePickingApplication.Interfaces;

import org.springframework.web.bind.annotation.PathVariable;

public interface IPickOrderPicker {

    public void updatePickOrder(@PathVariable Long id);
}
