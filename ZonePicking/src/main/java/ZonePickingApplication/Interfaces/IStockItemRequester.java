package ZonePickingApplication.Interfaces;

import ZonePickingApplication.PersistentClasses.StockItem;

public interface IStockItemRequester {

    public StockItem requestStockItem(Long materialId, Long requestedQuantity);
}
