package ZonePickingApplication.Interfaces;

import ZonePickingApplication.PersistentClasses.StockItem;

public interface IStockItemUpdater {

    public void updateStockItem(StockItem stockItem);
}
