package ZonePickingApplication.Interfaces;

import org.springframework.web.bind.annotation.PathVariable;

public interface ITransportUnitUpdater {

    public void updateTransportUnit(@PathVariable String barcode);

}
