package ZonePickingApplication.PersistentClasses;

import ZonePickingApplication.Enum.PickOrderState;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class PickOrderLine {

    @Id
    private Long id;
    private PickOrderState state;
    private Long pickOrderReferenceId;
    private Long materialId;
    private Long requestedQuantity;

    public PickOrderLine(@JsonProperty("state") PickOrderState state,
                         @JsonProperty("pickOrderReferenceId") Long pickOrderReferenceId,
                         @JsonProperty("materialId") Long materialId,
                         @JsonProperty("requestedQuantity") Long requestedQuantity) {
        this.state = state;
        this.pickOrderReferenceId = pickOrderReferenceId;
        this.materialId = materialId;
        this.requestedQuantity = requestedQuantity;
    }

    public PickOrderLine() {
    }
}
