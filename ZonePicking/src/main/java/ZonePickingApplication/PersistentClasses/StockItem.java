package ZonePickingApplication.PersistentClasses;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class StockItem { // TODO: StockItem probably does not need to be persistent

    @Id
    private Long id;
    private Long materialId;
    private Long quantity;
    private String transportUnitBarcode;

    public StockItem(@JsonProperty Long materialId,
                     @JsonProperty Long quantity,
                     @JsonProperty String transportUnitBarcode) {
        this.materialId = materialId;
        this.quantity = quantity;
        this.transportUnitBarcode = transportUnitBarcode;
    }

    public StockItem() {
    }
}
