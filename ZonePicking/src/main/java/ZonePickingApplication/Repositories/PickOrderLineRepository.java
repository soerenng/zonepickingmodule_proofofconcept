package ZonePickingApplication.Repositories;

import ZonePickingApplication.PersistentClasses.PickOrderLine;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PickOrderLineRepository extends JpaRepository<PickOrderLine, Long> {

    public List<PickOrderLine> findAllByPickOrderReferenceId(Long pickOrderRereferenceId);
}
