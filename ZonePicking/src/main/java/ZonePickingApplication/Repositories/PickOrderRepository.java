package ZonePickingApplication.Repositories;

import ZonePickingApplication.PersistentClasses.PickOrder;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface PickOrderRepository extends JpaRepository<PickOrder, Long> {

    List<PickOrder> findByTransportUnitBarcode(String transportUnitBarcode);

    Optional<PickOrder> findByPickOrderReferenceId(long pickOrderReferenceId);
}
