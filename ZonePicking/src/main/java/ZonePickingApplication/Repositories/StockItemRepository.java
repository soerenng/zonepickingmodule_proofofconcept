package ZonePickingApplication.Repositories;

import ZonePickingApplication.PersistentClasses.StockItem;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StockItemRepository extends JpaRepository<StockItem, Long> {

}
