package ZonePickingApplication.Repositories;

import ZonePickingApplication.PersistentClasses.TransportUnit;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface TransportUnitRepository extends JpaRepository<TransportUnit, Long> {
    public Optional<TransportUnit> findByBarcode(String barcode);
}
