package ZonePickingApplication;

import ZonePickingApplication.Components.PickOrderResourceAssembler;
import ZonePickingApplication.Controllers.PickOrderController;
import ZonePickingApplication.Enum.PickOrderState;
import ZonePickingApplication.Enum.PickOrderType;
import ZonePickingApplication.PersistentClasses.PickOrder;
import ZonePickingApplication.Repositories.PickOrderRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.HttpHeaders;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Arrays;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(PickOrderController.class)
@Import({PickOrderResourceAssembler.class})
public class PickOrderControllerTests {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private PickOrderRepository repository;


    @Test
    public void getShouldFetchAHalDocument() throws Exception {

        given(repository.findAll()).willReturn(
                Arrays.asList(
                        new PickOrder(PickOrderType.Zone, PickOrderState.New, "123456789"),
                        new PickOrder(PickOrderType.Zone, PickOrderState.Ready_To_Pick, "987654321")
                ));

        MvcResult mvcResult = mvc.perform(get("/orders").accept(MediaTypes.HAL_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaTypes.HAL_JSON + ";charset=UTF-8"))
                .andExpect(jsonPath("$._embedded.pickOrderList[0].type", is(PickOrderType.Zone.toString())))
                .andExpect(jsonPath("$._embedded.pickOrderList[0].state", is(PickOrderState.New.toString())))
                .andExpect(jsonPath("$._embedded.pickOrderList[0].transportUnitBarcode", is("123456789")))
                .andExpect(jsonPath("$._embedded.pickOrderList[1].type", is(PickOrderType.Zone.toString())))
                .andExpect(jsonPath("$._embedded.pickOrderList[1].state", is(PickOrderState.Ready_To_Pick.toString())))
                .andExpect(jsonPath("$._embedded.pickOrderList[1].transportUnitBarcode", is("987654321")))

                .andReturn();


    }

}